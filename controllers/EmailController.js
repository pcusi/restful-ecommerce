const nodemailer = require('nodemailer');
const Styliner = require('styliner');
const fs = require('fs');
const path = require('path');
const handlebars = require('handlebars');
const styliner = new Styliner(path.resolve(__dirname, '../templates/hbs'));

const emailTemplate = async () => {
    let file = fs.readFileSync(path.resolve(__dirname, '../templates/shopcartPayment.hbs'), 'utf8');
    let fileStyliner = styliner.processHTML(file).then(fileInline => {
        return fileInline;
    });

    return fileStyliner;
};

const replaceHTML = async (html, replacement) => {

    let template = handlebars.compile(html);

    let htmlTemplateReplaced = template(replacement);

    return htmlTemplateReplaced;
}

async function sendShopcartEmail(req, replacement) {

    const template = await emailTemplate();

    const templateReplacement = await replaceHTML(template, replacement);

    let transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 587,
        auth: {
            user: process.env.GOOGLE_USER,
            pass: process.env.GOOGLE_PASSWORD
        }
    });

    let { from, subject } = req.body;

    let email = await transporter.sendMail({
        from: from,
        to: `${replacement.email}`,
        subject: subject, // Subject line
        text: "Hello world?", // plain text body
        html: templateReplacement
    });

    if (email) return 'send';

    else return 'not-send';

}

module.exports = {
    sendShopcartEmail
}