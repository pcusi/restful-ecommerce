const stripe = require('stripe')(process.env.STRIPE_KEY);
const Payment = require('../schemas/PaymentSchema');
const User = require('../schemas/UserSchema');
const Product = require('../schemas/ProductSchema');
const Email = require('./EmailController');
const Coupon = require('../schemas/CouponSchema');
const { format } = require('date-fns');
const now = new Date();

async function shopcartPayment(req, res) {

    if (!req._id) return res.status(401).send({ message: 'Unauthorized.' });

    try {

        const { list_item, coupon } = req.body;

        const charge = await chargeCustomerCard(req, coupon, list_item);

        await shopcartCheckout(req, res, charge, list_item);

    } catch (e) {
        return res.status(400).send({ message: 'Something happens.' });
    }

}

//checkout shopping cart products and send an email to the customer
async function shopcartCheckout(req, res, charge, body) {

    if (charge.id != null) {

        const payment_id = await storePayment(charge, body, req, res);

        const user = await User.findById(req._id);

        const findPayment = await Payment.findById(payment_id);

        const emailReplacement = {
            names: `${user.names} ${user.lastnames}`,
            _id: findPayment._id,
            list_item: findPayment.shipment_order,
            email: user.email,
            address: user.address,
            order_date: format(now, 'MM/dd/yyyy'),
            shipping_method: 'Delivery',
            total: (Math.round(charge.amount) / 100).toFixed(2),
            subtotal: (Math.round(charge.subtotal) / 100).toFixed(2),
            receipt_url: charge.receipt_url,
            coupon: charge.coupon 
        }

        const emailSender = await Email.sendShopcartEmail(req, emailReplacement);

        if (emailSender == 'send' && payment_id != null) {

            return res.status(200).send({
                payment: `Your payment with ID: ${payment_id}, was completed.`,
                email: `Email was send to ${user.email}`
            });

        } else {

            return res.status(400).send({ message: 'Email or payment id doesn\'t exists.' });

        }

    }

    return res.status(400).send({ charge });

}

//save checkout to mongodb
async function storePayment(charge, body, req, res) {

    let payment = new Payment();

    payment.id = charge.id;
    payment.amount = charge.amount;
    payment.currency = charge.currency;
    payment.balance_transaction = charge.balance_transaction;
    payment.customer_id = charge.customer;
    payment.payment_method = charge.payment_method;
    payment.receipt_url = charge.receipt_url;
    payment.status = charge.status;
    payment.shipment_order = charge.shipment_order;
    payment.user_id = req._id;
    payment.list_item = body;

    let payment_id = await payment.save().then(item => {

        if (item) return item._id;

    }).catch(error => {

        return res.status(400).send({ message: error });

    });

    return payment_id;
}

//charge money to a customer card
async function chargeCustomerCard(req, coupon, list_item) {

    let discount = 0;

    const shipmentCart = await Product.find().where('_id').in(list_item).exec();

    const shipmentOrderQuantities = getProductsQuantities(list_item);

    const shipmentOrder = shipmentCart.map(cart => {

        const quantity = shipmentOrderQuantities.filter(order => order.id === cart.id).map(cart => cart.quantity);

        return {
            id: cart.id,
            price: cart.price,
            name: cart.name,
            description: cart.description,
            gallery: cart.gallery,
            quantity: quantity[0],
        }
    });

    const amount = parseFloat(shipmentOrder.reduce((a, { price, quantity }) => (a + price * quantity), 0));

    const findCoupon = await Coupon.findOne({ name: coupon });

    if (findCoupon != null) discount = (amount * 100) - (amount * (findCoupon.amount_percent / 100)) * 100;

    const charge = await stripe.charges.create({
        amount: findCoupon != null ? discount : (amount * 100),
        description: 'Product payment bag.',
        currency: 'PEN',
        customer: req.customer_id,
    }).then(charge => {

        return {
            id: charge.id,
            amount: charge.amount,
            subtotal: amount * 100,
            coupon: findCoupon != null ? findCoupon.amount_percent : null,
            currency: charge.currency,
            balance_transaction: charge.balance_transaction,
            customer: charge.customer,
            payment_method: charge.payment_method,
            receipt_url: charge.receipt_url,
            status: charge.status,
            shipment_order: shipmentOrder
        };

    }).catch(error => {
        return error;
    });

    return charge;

}

async function assignCustomerCard(req, res) {

    if (!req.customer_id) {

        return res.status(400).send({ message: 'Customer id no\'t found.' });

    }

    if (!req._id) {

        return res.status(403).send({ message: 'Unauthorized' });

    }

    const card = await stripe.customers.createSource(
        req.customer_id,
        {
            source: 'tok_visa',
        }
    );

    const user = await User.findByIdAndUpdate(req._id, { card_source: card.id });

    if (user) return res.status(200).send({ message: 'User card updated.', card });

    else return res.status(400).send({ message: 'User doesn\'t exists.' });

}

function getProductsQuantities(array) {

    const newArray = [];

    const count = {};

    array.forEach(function (i) {
        count[i] = (count[i] || 0) + 1
    });

    for (var i = 0; i < array.length; i++) {

        Object.keys(count)[i];

        let key = Object.keys(count)[i];

        let quantity = count[key];

        newArray.push({
            id: key,
            quantity: quantity
        });
    }

    return newArray.filter(x => x.quantity != undefined);

}

module.exports = {
    shopcartPayment,
    assignCustomerCard
}