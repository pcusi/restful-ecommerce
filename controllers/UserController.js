const User = require('../schemas/UserSchema');
const now = new Date();

async function updateUserInfoForShipment(req, res) {

    let { names, lastnames, address } = req.body;

    let userId = req._id;

    let userUpdated = await User.findByIdAndUpdate(userId, {
        names: names,
        lastnames: lastnames,
        address: address,
        updated_at: now.toISOString()
    });

    if (userUpdated) return res.status(200).send({ message: 'User updated.' });

    else return res.status(400).send({ message: 'User doesn\'t updated.' })

}

async function updateUserForSell(req, res) {

    let { company_name } = req.body;

    let userUpdated = await User.findByIdAndUpdate(req._id, {
        company_name: company_name,
        updated_at: now.toISOString()
    });

    if (userUpdated) return res.status(200).send({ message: 'User updated.' });

    else return res.status(400).send({ message: 'User doesn\'t updated.' })

}

module.exports = {
    updateUserInfoForShipment,
    updateUserForSell
}