const Product = require('../schemas/ProductSchema');
const path = require('path');
const multer = require('multer');
const cloudinary = require('../config/cloudinary');
const storage = multer.diskStorage({
    destination: (req, file, route) => {
        route(null, path.resolve(__dirname, '../upload/products/'));
    },
    filename: (req, file, route) => {
        route(null, file.originalname);
    }
});

async function createProduct(req, res) {

    let { name,
        description,
        price,
        discount_price,
        total_price,
        size,
        color,
        gallery,
        features
    } = req.body;

    let product = new Product();

    product.name = name;
    product.description = description;
    product.price = price;
    product.size = size || [];
    product.color = color || [];
    product.gallery = gallery || [];
    product.discount_price = discount_price || null;
    product.total_price = total_price || null;
    product.features = features || [];
    product.user_id = req._id;

    if (!req._id) {
        return res.status(403).send({ message: 'Unauthorized' });
    }

    product.save().then(item => {

        if (item) return res.status(200).send({ message: 'Product created' });

    }).catch(error => {

        return res.status(400).send({ message: error });

    })

}

async function uploadProductImage(req, res) {

    let upload = multer({
        storage: storage
    }).single('gallery');

    upload(req, res, async (error) => {

        if (error) return res.status(400).send({ error });

        let fileName = Math.round(Date.now() / 1000);
        let pathFile = req.file.path;

        cloudinary.uploader.upload(
            pathFile,
            {
                public_id: `products/${fileName}`,
                tags: 'products'
            },
            async (error, file) => {

                if (error) return res.status(400).send({ error });

                /*
                    let productUpdated = await Product.findByIdAndUpdate(productId, { $push: { gallery: file.url } });
                    if (productUpdated) return res.status(200).send({ product: productUpdated });
                */

                if (file) return res.status(200).send({ file: file.url });

                else return res.status(400).send({ message: 'Something happens.' });
            }
        )

    });

}

async function getProductById(req, res) {

    try {

        let productId = req.params.id;

        let findProduct = await Product.findById(productId);

        if (!productId) {

            return res.status(400).send({ message: 'Product no\'t found.' });

        } else {

            return res.status(200).send({ product: findProduct });

        }

    } catch (error) {

        return res.status(400).send({ error });

    }

}

async function getProductsByUser(req, res) {

    try {

        let findUserProducts = await Product.find({ user_id: req._id });

        if (!findUserProducts) {
            return res.status(400).send({ message: 'This user don\'t have products.' });
        }

        return res.status(200).send({ products: findUserProducts });

    } catch (error) {

        return res.status(400).send({ error: error });

    }

}

module.exports = {
    getProductsByUser,
    getProductById,
    uploadProductImage,
    createProduct
}