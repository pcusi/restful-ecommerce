const Coupon = require('../schemas/CouponSchema');

async function createCoupon(req, res) {

    let company_id = req.params.id;

    if (!req._id) return res.status(401).send({ message: 'Unauthorized.' });

    let { name, amount_percent, amount_tax } = req.body;

    let coupon = new Coupon();

    coupon.name = name;
    coupon.company_id = company_id;
    coupon.amount_percent = amount_percent;
    coupon.amount_tax = amount_tax || 0;

    await coupon.save()
        .then(item => res.status(200).send({ item }))
        .catch(error => res.status(400).send({ error }));


}

module.exports = {
    createCoupon
}