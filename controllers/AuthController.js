const User = require('../schemas/UserSchema');
const now = new Date();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const stripe = require('stripe')(process.env.STRIPE_KEY);
const EXPIRES_IN = process.env.EXPIRES_IN;

async function signUp(req, res) {

    let { email, password, name } = req.body;

    let findUser = await User.findOne({
        email: email
    });

    if (findUser) return res.status(400).send({ message: 'User exists, use a different email.' });

    const customer = await stripe.customers.create({
        email: email,
        name: name
    }).then(customer => {

        return customer;

    })

    let user = new User();

    user.email = customer.email;
    user.customer_id = customer.id;
    user.password = await bcrypt.hash(password, 9);

    user.save().then(item => {

        if (item) return res.status(200).send({ message: 'User created' });

    }).catch(error => {
        return res.status(400).send({ message: error });
    });

}

async function signUpLikeCompany(req, res) {

    let { email, password } = req.body;

    let findUser = await User.findOne({
        email: email
    });

    if (findUser) return res.status(400).send({ message: 'User exists, use a different email.' });

    let user = new User();

    user.email = email;
    user.password = await bcrypt.hash(password, 9);

    user.save().then(item => {

        if (item) return res.status(200).send({ message: 'User created' });

    }).catch(error => {
        return res.status(400).send({ message: error });
    });

}

async function signIn(req, res) {

    let { email, password } = req.body;

    let user = await User.findOne({ email });

    if (user) {

        let token = await authValidation(user.id, user.customer_id, password, user.password, res);

        if (token != '') return res.status(200).send({ token })

        else return res.status(400).send({ message: 'Passwords doesn\'t match' });

    } else {

        return res.status(400).send({ message: 'User doesn\'t exists, please sign up.' });

    }

}

async function authValidation(user_id, customer_id, password, hashPassword, res) {

    let validated = await bcrypt.compare(password, hashPassword);
    let token = '';
    let payload = {
        sub: user_id,
        customer_id: customer_id
    }

    if (validated) {

        token = jwt.sign(payload, process.env.TOKEN_SECRET, { expiresIn: EXPIRES_IN });

    }

    return token;

}

module.exports = {
    signUp,
    signUpLikeCompany,
    signIn
}