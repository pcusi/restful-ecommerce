const express = require('express');
const productController = require('../controllers/ProductController');
const router = express.Router();
const authMidd = require('../middlewares/auth');

router.post('/product', authMidd.ensureAuth, productController.createProduct);
router.post('/product/upload', authMidd.ensureAuth, productController.uploadProductImage);
router.get('/product/detail/:id', authMidd.ensureAuth, productController.getProductById);
router.get('/product/user', authMidd.ensureAuth, productController.getProductsByUser);

module.exports = router;