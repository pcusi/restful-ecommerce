const express = require('express');
const userController = require('../controllers/UserController');
const router = express.Router();
const authMidd = require('../middlewares/auth');

router.put('/user/shipment', authMidd.ensureAuth, userController.updateUserInfoForShipment);
router.put('/user/company', authMidd.ensureAuth, userController.updateUserForSell);

module.exports = router;