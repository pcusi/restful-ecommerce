const express = require('express');
const authController = require('../controllers/AuthController');
const router = express.Router();

router.post('/auth/sign-up', authController.signUp);
router.post('/auth/company/sign-up', authController.signUpLikeCompany);
router.post('/auth/sign-in', authController.signIn)

module.exports = router;