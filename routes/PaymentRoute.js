const express = require('express');
const paymentController = require('../controllers/PaymentController');
const router = express.Router();

const authMidd = require('../middlewares/auth');

router.post('/payment', authMidd.ensureAuth, paymentController.shopcartPayment);
router.post('/payment/card/assign', authMidd.ensureAuth, paymentController.assignCustomerCard);


module.exports = router;