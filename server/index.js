require('dotenv').config()

const express = require('express');
const app = express();
const cors = require('cors');

/* Middlewares */
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());

/* Swagger UI */
const swaggerUI = require('swagger-ui-express');
const swaggerJson = require('../swagger.json');

const port = process.env.PORT || 3500;

/* MongoDB connection */
const mongoose = require('mongoose');
const mongooseDB = mongoose.connect('mongodb://localhost:27017/ecommerce', {

});

/* routes */
const authRoute = require('../routes/AuthRoute');
const productRoute = require('../routes/ProductRoute');
const paymentRoute = require('../routes/PaymentRoute');
const userRoute = require('../routes/UserRoute');

app.use('/swagger/', swaggerUI.serve, swaggerUI.setup(swaggerJson));
app.use('/api/v1', [authRoute, productRoute, paymentRoute, userRoute]);

app.listen(port, async () => {

    await mongooseDB.then(() => {
        console.log('MongoDB connected');
    }).catch(error => {
        handleError(error);
    });

    console.log(`http://localhost:3500/api/v1/`);
});