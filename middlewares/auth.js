const jwt = require('jsonwebtoken');

exports.ensureAuth = function (req, res, next) {

    let authorization = req.headers.authorization;

    if (!authorization) {
        return res.status(403).send({ message: 'These requests needs Authorization Header' });
    }

    let token = authorization.split(" ")[1];

    jwt.verify(token, process.env.TOKEN_SECRET, (error, user) => {
        if (user) {
            req._id = user.sub;
            req.customer_id = user.customer_id;
            next();
        }

        if (error) return res.status(403).send({ message: 'Token expired. Please sign in again.', error: error });
    });
}