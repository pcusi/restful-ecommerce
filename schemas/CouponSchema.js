const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const now = new Date();

const CouponSchema = Schema({
    name: {
        type: String,
        required: true,
    },
    is_percent: {
        type: Boolean,
        default: true,
    },
    created_at: {
        type: String,
        default: now.toISOString(),
    },
    expires_in: {
        type: String,
        default: now.toISOString(),
    },
    company_id: {
        type: Schema.Types.ObjectId,
        ref: 'Company',
    },
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    amount_percent: {
        type: Number,
        default: 0,
        min: 0,
        max: 100
    },
    amount_tax: {
      type: Number,
      default: 0,
      min: 0,
      max: 4000,  
    },
});

module.exports = mongoose.model('Coupon', CouponSchema);