const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const now = new Date();

const UserSchema = Schema({
    email: {
        type: String,
        required: true,
    },
    company_name: {
        type: String,
        default: null
    },
    names: {
        type: String,
        default: null,
    },
    lastnames: {
        type: String,
        default: null,
    },
    address: {
        type: String,
        default: null,
    },
    password: {
        type: String,
        min: 3,
        max: 200
    },
    customer_id: {
        type: String,
    },
    country: {
        type: String,
        default: 'PER'
    },
    currency: {
        type: String,
        default: 'pen'
    },
    card_source: {
        type: String,
        default: null
    },
    created_at: {
        type: String,
        default: now.toISOString() 
    },
    updated_at: {
        type: String,
        default: null
    }
})

module.exports = mongoose.model('User', UserSchema);