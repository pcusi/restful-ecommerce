const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const now = new Date();

const ProductSchema = Schema({
    name: {
        type: String,
    },
    price: {
        type: Number,
        required: true
    },
    discount_price: {
        type: Number,
        default: null
    },
    total_price: {
        type: Number,
        default: null
    },
    description: {
        type: String,
        min: 20,
        max: 200
    },
    size: [
        {
            type: String,
            default: []
        },
    ],
    color: [
        {
            type: String,
            default: []
        }
    ],
    created_at: {
        type: String,
        default: now.toISOString() 
    },
    updated_at: {
        type: String,
        default: null
    },
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    features: [
        {
            type: String,
            default: []
        }
    ],
    gallery: [
        {
            type: String,
            default: []
        }
    ]
})

module.exports = mongoose.model('Product', ProductSchema);