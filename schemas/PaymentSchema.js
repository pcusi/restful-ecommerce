const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PaymentSchema = Schema({
    id: {
        type: String
    },
    amount: {
        type: Number
    },
    currency: {
        type: String
    },
    balance_transactiom: {
        type: String
    },
    customer_id: {
        type: String
    },
    payment_method: {
        type: String
    },
    receipt_url: {
        type: String
    },
    status: {
        type: String
    },
    list_item: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Product',
            default: []
        }
    ],
    shipment_order: Array,
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
})

module.exports = mongoose.model('Payment', PaymentSchema);